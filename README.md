<div align="center">
<img src="./logo.png" width="300" >
<h1>Processo Seletivo 20.2</h1>
<h2>Tarefa CSS</h2>
<h2><a href="https://pksasso.gitlab.io/aula-css-3-in/">Live here</a></h2>
</div>

Para esse final de semana teremos uma tarefa de CSS, esta tem como objetivo fazer vocês internalizarem e exercitarem o conteúdo passado com algo real;

Observações:
  - Os links serão apontados para o próprio documento com "#"
  - O mapa não precisa ser funcional
  - Se atente aos detalhes :)
  - O site deve ser responsivo

## Objetivo - Clonar o layout abaixo
<div align="center">
<img src="./layout.png" width="300" >
</div>